//
//  DemoARKitApp.swift
//  DemoARKit
//
//  Created by Trịnh Nhật on 10/12/2020.
//

import SwiftUI

@main
struct DemoARKitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
